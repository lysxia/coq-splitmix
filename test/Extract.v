From Coq Require Extraction ExtrOCamlInt63.
From Coq Require Import ZArith Int63.
From Int64 Require Import Int64.
From Splitmix Require Import Splitmix.

Extraction Blacklist Int64.

Module Import SlowSplitmix.

Local Open Scope int64.

Record state := MkState {
  seed : int64;
  gamma : int64;
}.

Definition of_seed (n : int64) : state :=
  {| seed := n;
     gamma := golden_gamma;
  |}.

Definition mk_split seed1 seed2 :=
  {| seed := mix64_variant13 seed1
   ; gamma := mix_gamma seed2
  |}.

Definition split '(MkState seed0 gamma) : state * state :=
  let seed1 := seed0 + gamma in
  let seed2 := seed1 + gamma in
  let rng1 := mk_split seed1 seed2 in
  let rng2 := MkState seed2 gamma in
  (rng1, rng2).

Definition bits '(MkState seed gamma) :=
  mix64_variant13 (seed + gamma).

End SlowSplitmix.

Definition state_of_int (x : int64) : state :=
  of_seed x.

Definition int_of_state (s : state) : int64 :=
  bits s.

Definition coq_split := split.

Extraction "CoqSplitmix.ml" coq_split state_of_int int_of_state.
