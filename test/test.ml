(* Compare output with the pure-splitmix package. *)

exception FailTest of string

let compare_constant () =
  let x = CoqSplitmix.x_bf58476d1ce4e5b9 in
  let y = 0xbf58476d1ce4e5b9L in
  if x <> y then
    raise (FailTest (Printf.sprintf "constants: %Ld %Ld" x y))
;;

let show3 s i x y =
  Printf.sprintf "%s(%Ld): %Ld <> %Ld" s i x y

let compare_mix_ ~mix1 ~mix2 =
  for _ = 0 to 100000 do
    let i = Random.int64 Int64.max_int in
    let x = mix1 i in
    let y = mix2 i in
    if x <> y then raise (FailTest (show3 "mix64" i x y))
  done

let compare_mix64 () =
  compare_mix_ ~mix1:PureSplitMix.Internal.mix64 ~mix2:CoqSplitmix.mix64

let compare_mix64variant13 () =
  compare_mix_ ~mix1:PureSplitMix.Internal.mix64_variant13 ~mix2:CoqSplitmix.mix64_variant13

let dump_prng ~split ~bits ~of_seed seed path =
  let state0 = of_seed seed in
  let rec dump state path =
    let n = bits state in
    let (s1, s2) = split state in
    let more =
      match path with
      | true  :: path -> dump s1 path
      | false :: path -> dump s2 path
      | [] -> []
    in n :: more
  in dump state0 path
;;

let show_dump xs =
  List.iter (Printf.printf " %Ld") xs;
  Printf.printf "\n"

let compare_ seed path =
  let coq_dump =
    let open CoqSplitmix in
    dump_prng
      ~split:coq_split
      ~bits:int_of_state
      ~of_seed:state_of_int
      seed path
  in
  let ocaml_dump =
    let open PureSplitMix in
    let bits s = fst (next_int64 s) in
    dump_prng ~split ~bits ~of_seed seed path
  in
  if coq_dump = ocaml_dump then
    ()
  else (
    Printf.printf "Seed: %Ld\n" seed;
    show_dump coq_dump;
    show_dump ocaml_dump;
    failwith "Not the same"
  )

let test () =
  Random.self_init ();
  for _ = 0 to 1000 do
    let seed = Random.int64 Int64.max_int in
    let path = List.init 1000 (fun _ -> Random.bool ()) in
    compare_ seed path
  done

let million ~split ~bits ~of_seed seed =
  let rec go state depth =
    if depth = 0 then
      Int64.to_int (bits state)
    else
      let (s1, s2) = split state in
      go s1 (depth - 1) + go s2 (depth - 1)
  in
  go (of_seed seed) 20

let bench_coq () =
  let open CoqSplitmix in
  ignore (million
    ~split:coq_split
    ~bits:int_of_state
    ~of_seed:state_of_int
    33L)

let bench_ocaml () =
  let open PureSplitMix in
  let bits s = fst (next_int64 s) in
  ignore (million ~split ~bits ~of_seed 33L)

let () =
  compare_constant ();
  compare_mix64 ();
  compare_mix64variant13 ();
  test ()
